import { Component, OnInit } from '@angular/core';

import { DeviceService } from './services/device.service';

@Component({
    selector: 'my-app',
    templateUrl: './app/app.component.html',
    providers: [ DeviceService ]
})
export class AppComponent implements OnInit {

    constructor(
        private deviceService: DeviceService
    ) { }

    ngOnInit() {
        this.refreshList();
    }

    refreshList() {
        this.loadSavedClient();
        this.deviceService.scanForClientsAndGetData();
    }

    loadSavedClient() :void {
        this.deviceService.getClients()
        .then(clients => {
            //this.getDataForClients(clients);
        });
    }
}
