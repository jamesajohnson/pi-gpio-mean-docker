import { Injectable } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map'

@Injectable()
export class DeviceService {

    scanning: boolean = false;

    //object of all clients --< rooms --< devices
    clientRoomDevices: Object[] = [];

    constructor(
        public http: Http
    ) { }

    getPinStatus(pin :string, clientIP :string): Promise<string> {
        return this.http
        .get("http://" + clientIP + ":3000/pin/" + pin)
        .toPromise()
        .then(data => data['_body'] as string);
    }

    /*===========================
            Client Methods
    =============================*/

    newClient(ip :string, name :string): Promise<Object> {
        return this.http
        .post("http://" + window.location.hostname + ":3000/client/", { ip, name })
        .toPromise();
    }

    getClients(clientIP :string = window.location.hostname): Promise<Object[]> {
        return this.http
        .get("http://" + clientIP + ":3000/clients")
        .toPromise()
        .then(data => data.json() as Object[]);
    }

    searchClients(): Promise<string[]> {
        return this.http
        .get("http://" + window.location.hostname + ":3000/search/clients")
        .toPromise()
        .then(data => data.json() as string[]);
    }

    deleteClient(id :string, clientIP :string): Promise<Object> {
        return this.http
        .delete("http://" + clientIP + ":3000/client/" + id)
        .toPromise()
        .then(data => data.json() as Object[]);
    }

    /*===========================
            Device Methods
    =============================*/

    switchDevice(pin :string, clientIP :string): Promise<Object> {
        return this.http
        .get("http://" + clientIP + ":3000/pin/" + pin + "/switch")
        .toPromise()
        .then(data => data.json())
        .catch(this.logError);
    }

    newDevice(name :string, pin :string, roomID :string, clientIP :string): Promise<Object> {
        return this.http
        .post("http://" + clientIP + ":3000/device/", { name, pin, roomID })
        .toPromise();
    }

    getDevices(roomID :string, clientIP :string): Promise<Object[]> {
        console.log("Getting devices for " + clientIP + " room " + roomID);
        return this.http
        .get("http://" + clientIP + ":3000/room/" + roomID + "/devices")
        .toPromise()
        .then(data => data.json() as Object[]);
    }

    deleteDevice(id :string, clientIP :string): Promise<Object> {
        return this.http
        .delete("http://" + clientIP + ":3000/device/" + id)
        .toPromise()
        .then(data => data.json() as Object[]);
    }

    /*===========================
            Room Methods
    =============================*/

    newRoom(name :string, clientIP: string): Promise<Object> {
        return this.http
        .post("http://" + clientIP + ":3000/room/", { name })
        .toPromise();
    }

    getRooms(clientIP :string): Promise<Object[]> {
        console.log("Getting rooms for " + clientIP);
        return this.http
        .get("http://" + clientIP + ":3000/rooms")
        .toPromise()
        .then(data => data.json() as Object[]);
    }

    deleteRoom(id :string, clientIP :string): Promise<Object> {
        return this.http
        .delete("http://" + clientIP + ":3000/room/" + id)
        .toPromise()
        .then(data => data.json() as Object[]);
    }

    logError(err :any) {
        console.error('There was an error: ' + err);
    }

    /*==========================
            Processing
    ===========================*/

    refresh() {
        this.clientRoomDevices = [];
        this.scanForClientsAndGetData();
        this.loadClientsAndGetData();
    }

    scanForClientsAndGetData() {
        this.searchClients()
        .then(clients => {
            for(let client of clients) {
                console.log("array position: ");
                var inArray = this.clientRoomDevices.filter(savedClient => savedClient['ip'] == client);
                console.log(inArray);
                if(inArray.length == 0) {
                    this.getDataForClient(client);
                }
            }
        });
    }

    loadClientsAndGetData() {
        this.getClients()
        .then(clients => {
            console.log("loadClientsandGetData");
            for(let client of clients) {
                console.log(client);
                this.getDataForClient(client['ip']);
            }
        });
    }

    getDataForClient(client :string) {
        console.log("getDataForClient");
        console.log(client);
        let clientInfo: string[] = [];

        this.getClients(client)
        .then(clients => {

            clientInfo['name'] = clients[0]['name'];
            clientInfo['ip'] = clients[0]['ip'];
            clientInfo['_id'] = clients[0]['_id'];
            clientInfo['rooms'] = [];

            this.getRooms(client)
            .then(rooms => {

                for(let room of rooms) {

                    let roomInfo :string[] = [];
                    roomInfo['name'] = room['name'];
                    roomInfo['_id'] = room['_id'];
                    roomInfo['devices'] = [];

                    this.getDevices(room["_id"], client)
                    .then(devices => {

                        for(let device of devices) {

                            let deviceInfo :string[] = [];
                            deviceInfo['name'] = device['name'];
                            deviceInfo['pin'] = device['pin'];
                            deviceInfo['status'] = 1;
                            deviceInfo['_id'] = device['_id'];

                            roomInfo['devices'].push(deviceInfo);
                        }

                        clientInfo['rooms'].push(roomInfo);
                    });
                }

                console.log(clientInfo);
                var inArray = this.clientRoomDevices.filter(savedClient => savedClient['ip'] == clients[0]['ip']);
                if(inArray.length == 0) {
                    console.log("Saving client: " + clients[0]['ip'] + " to the database");
                    //this.newClient(clients[0]['ip'], clients[0]['name']);
                }
                this.clientRoomDevices.push(clientInfo);
            })
        });
    }
}
