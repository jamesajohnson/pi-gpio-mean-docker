import { Component, ViewChild, OnInit } from '@angular/core';

import { DeviceService } from '../../services/device.service';

@Component({
    selector: 'app-manage',
    templateUrl: './app/components/manage/manage.component.html',
    styleUrls: ['./app/components/manage/manage.component.css']
})
export class ManageComponent implements OnInit {

    @ViewChild('newRoomName') newRoomName :any;
    newRoomClientIP :string;

    @ViewChild('newDeviceName') newDeviceName :any;
    @ViewChild('newDevicePin') newDevicePin :any;

    newClientIP :string;
    @ViewChild('newClientName') newClientName :any;

    @ViewChild('initialSetupDialog') initialSetupDialog :any;
    thisClientName :string;

    //rooms
    editingRoom: string;
    editingClient: string;

    constructor(
        private deviceService: DeviceService
    ) { }

    ngOnInit() {
        this.deviceService.getClients()
        .then(clients => {
            if(clients.length == 0) {
                this.initialSetupDialog.show();
            }
        })
    }

    refreshList() :void {
        this.deviceService.refresh();
    }

    // name this client on first boot
    saveConfig() :void {
        this.deviceService.newClient(
            window.location.hostname,
            this.thisClientName
        )
        .then(response => {
            this.initialSetupDialog.close();
            console.log(response);
            this.refreshList();
        });
    }

    /*=====================
        Client Methods
    ======================*/

    saveNewClient() :void {
        this.deviceService.newClient(this.newClientIP, this.newClientName['value'])
        .then(response => {
            console.log(response);
        });
    }

    deleteClient(id :string, clientIP:string) :void {
        this.deviceService.deleteClient(id, clientIP)
        .then(response => {
            console.log(response);
        });
    }

    /*=====================
        Room Methods
    ======================*/

    saveNewRoom() :void {
        console.log("saving room: " + this.newRoomName['value'] + " to client: " + this.newRoomClientIP);
        this.deviceService.newRoom(this.newRoomName['value'], this.newRoomClientIP)
        .then(response => {
            console.log(response);
        });
    }

    deleteRoom(id :string, clientIP :string) :void {
        console.log("deleting: " + id + " on client: " + clientIP);
        this.deviceService.deleteRoom(id, clientIP)
        .then(response => {
            console.log(response);
        });
    }

    /*=====================
        Device Methods
    ======================*/

    saveNewDevice() :void {
        console.log('Saving Device: ' + this.newDeviceName['value'] + ', pin: ' + this.newDevicePin['value'] + ' to room : ' + this.editingRoom + " on device: " + this.editingClient);
        this.deviceService.newDevice(this.newDeviceName['value'], this.newDevicePin['value'], this.editingRoom, this.editingClient)
        .then(response => {
            console.log(response);
        });
    }

    deleteDevice(id :string, clientIP :string) :void {
        console.log("deleting device: " + id + " from: " + clientIP);
        this.deviceService.deleteDevice(id, clientIP)
        .then(response => {
            console.log(response);
        });
    }

}
