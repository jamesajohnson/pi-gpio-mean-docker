import { Component, ViewChild, OnInit } from '@angular/core';

import { DeviceService } from '../../services/device.service';

@Component({
    selector: 'app-dash',
    templateUrl: './app/components/dash/dash.component.html',
    styleUrls: ['./app/components/dash/dash.component.css']
})
export class DashComponent implements OnInit {

    clientRoomDevices :Object[] = [];

    constructor(
        private deviceService: DeviceService
    ) { }

    ngOnInit() {
    }

    switchDevice(pin: string, clientIP: string){
        this.deviceService.switchDevice(pin, clientIP)
        .then(response => {
            console.log(response);
        });
    }
}
