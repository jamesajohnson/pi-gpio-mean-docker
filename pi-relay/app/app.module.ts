import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';
import { MdlModule } from 'angular2-mdl';

import { AppComponent } from './app.component';
import { DeviceService } from './services/device.service';
import { DashComponent } from './components/dash/dash.component';
import { RoomDevicesPipe } from './pipes/room-devices.pipe';
import { RoomPipe } from './pipes/room.pipe';
import { ManageComponent } from './components/manage/manage.component';

const appRoutes: Routes = [
  { path: '', component: DashComponent },
  { path: 'manage', component: ManageComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    DashComponent,
    RoomDevicesPipe,
    RoomPipe,
    ManageComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes),
    MdlModule
  ],
  bootstrap: [
      AppComponent
  ]
})
export class AppModule { }
