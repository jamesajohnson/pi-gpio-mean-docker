import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'roomDevices'
})
export class RoomDevicesPipe implements PipeTransform {

  transform(devices: Object[], args?: any): any {
    return devices.filter( device => device['roomID'] == args);
  }

}
