import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'room'
})
export class RoomPipe implements PipeTransform {

  transform(rooms: Object[], args?: any): any {
    var filtered = rooms.filter(room => room['_id'] == args);
    console.log(filtered);
    return filtered;
  }

}
