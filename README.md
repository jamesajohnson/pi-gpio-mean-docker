# Pi Relay

### What is this?
Built using; Angular 2, Express, Mongo and Docker. Used to serve a local site for switching Raspberry Pi GPIO pins on and off.

The Angular 2 app isn't built with Angular CLI because Angular CLI requires Node SASS and Node SASS won't run on ARM. Instead it's built using the [quickstart](https://github.com/angular/quickstart) template.

### How to run?
Install [Docker](https://www.raspberrypi.org/blog/docker-comes-to-raspberry-pi/) and [Docker Compose](https://github.com/hypriot/arm-compose#installation) onto your Raspberry Pi.

The Dockerfile for `jamesajohnson/rpi-node` is also in the `rpi-node` folder.

Run `sudo docker-compose up --build` to build and start the app. Angular app will run on `*raspberry-pi-ip*:80`. Express will run on `*raspberry-pi-ip*:3000`.

### Whats Next?
- multiple Raspberry Pi connections (Multi Room)
- Get devices stored in a database, not just an array.
