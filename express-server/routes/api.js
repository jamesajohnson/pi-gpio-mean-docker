// Import dependencies
const mongoose = require('mongoose');
const express = require('express');
var sys = require('sys')
var exec = require('child_process').exec;
var child;
const router = express.Router();

// MongoDB URL from the docker-compose file
const dbHost = 'mongodb://database/mean-docker';

// Connect to mongodb
mongoose.connect(dbHost);

// create mongoose schema
const roomSchema = new mongoose.Schema({
    name: String                                                //name of the room
});

const deviceSchema = new mongoose.Schema({
    name: String,                                               //name of the device
    pin: String,                                                //the GPIO pin number of the device
    roomID: String                                              //the room the device is in
});

const clientSchema = new mongoose.Schema({
    ip: String,                                                 //the clients IP address
    name: String                                                //name of the client
});

const clientPositionSchema = new mongoose.Schema({
    mainServer: String                                          //The IP of the main server
});

// create mongoose model
const Room = mongoose.model('room', roomSchema);
const Device = mongoose.model('device', deviceSchema);
const Client = mongoose.model('client', clientSchema);
const ClientPosition = mongoose.model('client_position', clientPositionSchema);

/* GET api listing. */
router.get('/', (req, res) => {
    res.send('api works');
});

/*
* Client Server Methods
*/

//Search for other Pi relay devices on the network TODO make this better!
router.get('/search/clients', (req, res) => {
    console.log("Scanning for clients");
    child = exec(" nmap -sS -p 3000 192.168.1.0/24 -oG - | awk '/Up$/{print $2}'", function (error, stdout, stderr) {
        list = stdout.replace(/(\r\n|\n|\r)/gm,",");
        list = list.trim();
        list = list.split(",");
        res.status(200).send( JSON.stringify(list) );
        if (error !== null) {
            console.log(error);
            res.status(400).json(error);
        }
    });
});

//return all saved clients
router.get('/clients', (req, res) => {
    Client.find({}, (err, devices) => {
        if (err) res.status(500).send(error)

        res.status(200).json(devices);
    });
});

//Add a client to the list of clients
router.post('/client', (req, res) => {
    let client = new Client({
        ip: req.body.ip,
        name: req.body.name
    });

    client.save(error => {
        if (error) res.status(500).send(error);

        res.status(201).json({
            message: 'Client created successfully'
        });
    });
});

// delete a client
router.delete('/client/:id', (req, res) => {
    var id = mongoose.Types.ObjectId(req.params.id);
    Client.remove({'_id': id}, (err, device) => {
        if (err) res.status(500).send(error)

        res.status(202).json(device);
    });
});

/*
* Pin Methods
*/

// get pin status
router.get('/pin/:pin', (req, res) => {
    //TODO stop CLI here, e.g. /pin/1 && whoami
    let pin = req.params.pin;
    if (pin)
    {
        child = exec("sudo gpio read " + pin, function (error, stdout, stderr) {
            //get, switch and write pin state
            res.status(200).send(stdout[0]);
            if (error !== null) {
                console.log(error);
                res.status(400).json(error);
            }
        });
    }
});

//switch pin status TODO matke this http PATCH
router.get('/pin/:pin/switch', (req, res) => {
    //TODO stop CLI here, e.g. /pin/1 && whoami
    let pin = req.params.pin;
    console.log('got pin: ' + pin);
    if (pin)
    {
        //set pin mode out
        child = exec("sudo gpio mode " + pin + " out", function (error, stdout, stderr) {
            if (error !== null) {
                console.log('exec error: ' + error);
            }
        });

        let val = 1;
        child = exec("sudo gpio read " + pin, function (error, stdout, stderr) {
            //get, switch and write pin state
            val = stdout;
            val ^= 1;
            child = exec("sudo gpio write " + pin + " " + val, function (error, stdout, stderr) {
                res.status(200).json('updated');
                if (error !== null) {
                    console.log(error);
                    es.status(400).json(error);
                }
            });
            if (error !== null) {
                console.log(error);
                res.status(400).json(error);
            }
        });
    }
});

/*
*   Device Methods
*/

// get all devices
router.get('/devices', (req, res) => {
    Device.find({}, (err, devices) => {
        if (err) res.status(500).send(error)

        res.status(200).json(devices);
    });
});

// create new device
router.post('/device', (req, res) => {
    let devices = new Device({
        name: req.body.name,
        pin: req.body.pin,
        roomID: req.body.roomID
    });

    devices.save(error => {
        if (error) res.status(500).send(error);

        res.status(201).json({
            message: 'Device created successfully'
        });
    });
});

// delete a room
router.delete('/device/:id', (req, res) => {
    var id = mongoose.Types.ObjectId(req.params.id);
    Device.remove({'_id': id}, (err, device) => {
        if (err) res.status(500).send(error)

        res.status(202).json(device);
    });
});

/*
*   Room Methods
*/

// get all rooms
router.get('/rooms', (req, res) => {
    Room.find({}, (err, rooms) => {
        if (err) res.status(500).send(error)

        res.status(200).json(rooms);
    });
});

router.get('/room/:id/devices', (req, res) => {
    var id = mongoose.Types.ObjectId(req.params.id);
    Device.find({'roomID':id}, (err, devices) => {
        if (err) res.status(500).send(error)

        res.status(200).json(devices);
    });
});

// create new room
router.post('/room', (req, res) => {
    let room = new Room({
        name: req.body.name
    });

    room.save(error => {
        if (error) res.status(500).send(error);

        res.status(201).json({
            message: 'Room created successfully'
        });
    });
});

// delete a room
router.delete('/room/:id', (req, res) => {
    var id = mongoose.Types.ObjectId(req.params.id);
    Device.remove({'roomID':id}, (err, devices) => {
        if (err) res.status(500).send(error)

        Room.remove({'_id': id}, (err, room) => {
            if (err) res.status(500).send(error)

            res.status(202).json(room);
        });
    });
});

module.exports = router;
